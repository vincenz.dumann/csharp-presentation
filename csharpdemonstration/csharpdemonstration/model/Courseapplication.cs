﻿using System;
using System.Collections.Generic;

#nullable disable

namespace csharpdemonstration.model
{
    public partial class Courseapplication
    {
        public int Id { get; set; }
        public int Student { get; set; }
        public int Course { get; set; }

        public virtual Course CourseNavigation { get; set; }
        public virtual Studend StudentNavigation { get; set; }
    }
}
