﻿using System;
using System.Collections.Generic;

#nullable disable

namespace csharpdemonstration.model
{
    public partial class Professor
    {
        public Professor()
        {
            Courses = new HashSet<Course>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Forename { get; set; }
        public int Gender { get; set; }
        public int Faculty { get; set; }

        public virtual Studyfield FacultyNavigation { get; set; }
        public virtual Gender GenderNavigation { get; set; }
        public virtual ICollection<Course> Courses { get; set; }
    }
}
