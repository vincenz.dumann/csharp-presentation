﻿using System;
using System.Collections.Generic;

#nullable disable

namespace csharpdemonstration.model
{
    public partial class Gender
    {
        public Gender()
        {
            Professors = new HashSet<Professor>();
            Studends = new HashSet<Studend>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Professor> Professors { get; set; }
        public virtual ICollection<Studend> Studends { get; set; }
    }
}
