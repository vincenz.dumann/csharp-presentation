﻿using System;
using System.Collections.Generic;

#nullable disable

namespace csharpdemonstration.model
{
    public partial class Studend
    {
        public Studend()
        {
            Courseapplications = new HashSet<Courseapplication>();
        }

        public int Id { get; set; }
        public int Matrnr { get; set; }
        public string Name { get; set; }
        public string Forename { get; set; }
        public int Gender { get; set; }
        public int Studybranch { get; set; }

        public virtual Gender GenderNavigation { get; set; }
        public virtual Studyfield StudybranchNavigation { get; set; }
        public virtual ICollection<Courseapplication> Courseapplications { get; set; }
    }
}
