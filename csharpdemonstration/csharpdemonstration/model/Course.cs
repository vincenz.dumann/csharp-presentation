﻿using System;
using System.Collections.Generic;

#nullable disable

namespace csharpdemonstration.model
{
    public partial class Course
    {
        public Course()
        {
            Courseapplications = new HashSet<Courseapplication>();
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public int Professor { get; set; }

        public virtual Professor ProfessorNavigation { get; set; }
        public virtual ICollection<Courseapplication> Courseapplications { get; set; }
    }
}
