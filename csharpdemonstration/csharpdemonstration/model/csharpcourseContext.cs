﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

#nullable disable

namespace csharpdemonstration.model
{
    public partial class csharpcourseContext : DbContext
    {
        public csharpcourseContext()
        {
        }

        public csharpcourseContext(DbContextOptions<csharpcourseContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Course> Courses { get; set; }
        public virtual DbSet<Courseapplication> Courseapplications { get; set; }
        public virtual DbSet<Gender> Genders { get; set; }
        public virtual DbSet<Professor> Professors { get; set; }
        public virtual DbSet<Studend> Studends { get; set; }
        public virtual DbSet<Studyfield> Studyfields { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. You can avoid scaffolding the connection string by using the Name= syntax to read it from configuration - see https://go.microsoft.com/fwlink/?linkid=2131148. For more guidance on storing connection strings, see http://go.microsoft.com/fwlink/?LinkId=723263.
                optionsBuilder.UseMySql("server=localhost;user=root;database=csharpcourse", Microsoft.EntityFrameworkCore.ServerVersion.Parse("10.4.22-mariadb"));
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasCharSet("utf8mb4")
                .UseCollation("utf8mb4_general_ci");

            modelBuilder.Entity<Course>(entity =>
            {
                entity.ToTable("course");

                entity.HasIndex(e => e.Professor, "FK_course_professor");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("code")
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("name")
                    .HasDefaultValueSql("''");

                entity.Property(e => e.Professor)
                    .HasColumnType("int(11)")
                    .HasColumnName("professor");

                entity.HasOne(d => d.ProfessorNavigation)
                    .WithMany(p => p.Courses)
                    .HasForeignKey(d => d.Professor)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_course_professor");
            });

            modelBuilder.Entity<Courseapplication>(entity =>
            {
                entity.ToTable("courseapplication");

                entity.HasIndex(e => e.Course, "FK_courseapplication_course");

                entity.HasIndex(e => e.Student, "FK_courseapplication_studend");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id");

                entity.Property(e => e.Course)
                    .HasColumnType("int(11)")
                    .HasColumnName("course");

                entity.Property(e => e.Student)
                    .HasColumnType("int(11)")
                    .HasColumnName("student");

                entity.HasOne(d => d.CourseNavigation)
                    .WithMany(p => p.Courseapplications)
                    .HasForeignKey(d => d.Course)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_courseapplication_course");

                entity.HasOne(d => d.StudentNavigation)
                    .WithMany(p => p.Courseapplications)
                    .HasForeignKey(d => d.Student)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_courseapplication_studend");
            });

            modelBuilder.Entity<Gender>(entity =>
            {
                entity.ToTable("genders");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.Code)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("code");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("name");
            });

            modelBuilder.Entity<Professor>(entity =>
            {
                entity.ToTable("professor");

                entity.HasIndex(e => e.Gender, "FK_professor_genders");

                entity.HasIndex(e => e.Faculty, "FK_professor_studyfields");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id");

                entity.Property(e => e.Faculty)
                    .HasColumnType("int(11)")
                    .HasColumnName("faculty");

                entity.Property(e => e.Forename)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("forename");

                entity.Property(e => e.Gender)
                    .HasColumnType("int(11)")
                    .HasColumnName("gender");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("name");

                entity.HasOne(d => d.FacultyNavigation)
                    .WithMany(p => p.Professors)
                    .HasForeignKey(d => d.Faculty)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_professor_studyfields");

                entity.HasOne(d => d.GenderNavigation)
                    .WithMany(p => p.Professors)
                    .HasForeignKey(d => d.Gender)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_professor_genders");
            });

            modelBuilder.Entity<Studend>(entity =>
            {
                entity.ToTable("studend");

                entity.HasIndex(e => e.Gender, "FK_studend_genders");

                entity.HasIndex(e => e.Studybranch, "FK_studend_studyfields");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .HasColumnName("id");

                entity.Property(e => e.Forename)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("forename");

                entity.Property(e => e.Gender)
                    .HasColumnType("int(11)")
                    .HasColumnName("gender");

                entity.Property(e => e.Matrnr)
                    .HasColumnType("int(11)")
                    .HasColumnName("matrnr");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .HasColumnName("name");

                entity.Property(e => e.Studybranch)
                    .HasColumnType("int(11)")
                    .HasColumnName("studybranch");

                entity.HasOne(d => d.GenderNavigation)
                    .WithMany(p => p.Studends)
                    .HasForeignKey(d => d.Gender)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_studend_genders");

                entity.HasOne(d => d.StudybranchNavigation)
                    .WithMany(p => p.Studends)
                    .HasForeignKey(d => d.Studybranch)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_studend_studyfields");
            });

            modelBuilder.Entity<Studyfield>(entity =>
            {
                entity.ToTable("studyfields");

                entity.Property(e => e.Id)
                    .HasColumnType("int(11)")
                    .ValueGeneratedNever()
                    .HasColumnName("id");

                entity.Property(e => e.Code)
                    .HasMaxLength(3)
                    .HasColumnName("code");

                entity.Property(e => e.Name)
                    .HasMaxLength(50)
                    .HasColumnName("name");
            });

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
